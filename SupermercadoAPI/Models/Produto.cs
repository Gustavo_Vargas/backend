﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SupermercadoAPI.Models
{
    public class Produto
    {
        //PK
        public int Id { get; set; }

        [Required()] //Define obrigatoriedade
        [MinLength(10)]
        [MaxLength(300)] //Define tamanho
        public string Descricao { get; set; } //Descrição do produto

        [Required()] //Define obrigatoriedade
        public double Preco { get; set; } //Preço do produto

        [Required()] //Define obrigatoriedade
        public int Quantidade { get; set; } //Define a quantidade do produto

        public Fornecedor Fornecedor { get; set; } //Relação com Fornecedor
    }
}
