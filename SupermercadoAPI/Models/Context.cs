﻿using Microsoft.EntityFrameworkCore;
using SupermercadoAPI.Models;

namespace SupermercadoAPI.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }
        //Tabela de Produtos
        public DbSet<Produto> Produtos { get; set; }
        //Tabela de Fornecedores
        public DbSet<Fornecedor> Fornecedores { get; set; }
    }
}
