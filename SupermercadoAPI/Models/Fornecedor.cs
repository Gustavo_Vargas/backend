﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SupermercadoAPI.Models
{
    public class Fornecedor
    {
        //PK
        public int Id { get; set; }

        [MinLength(10)]
        [MaxLength(30)] //Define tamanho
        public string Nome { get; set; } //Define o nome do fornecedor
    }
}
