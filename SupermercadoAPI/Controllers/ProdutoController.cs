﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupermercadoAPI.Models;

namespace SupermercadoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly Context _context;

        public ProdutoController(Context context)
        {
            _context = context;
        }

        // GET: api/Produto
        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<IEnumerable<Produto>>> GetProdutos()
        {
            return await _context.Produtos.Include(f => f.Fornecedor).ToListAsync(); //Retorna a lista de produtos

        }

        // GET: api/Produto/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Produto>> GetProduto(int id)
        {
            //Busca produto por Id
            var produto = await _context.Produtos.FindAsync(id);

            //Se não contrado
            if (produto == null)
            {
                return NotFound(); //Retorna não encontrado
            }

            return produto; //Retorna as informações do produto
        }

        // PUT: api/Produto/id
        [HttpPut()]
        public async Task<IActionResult> PutProduto([FromBody] Produto produto)
        {
            //Faz a busca do produto no banco, conforme Id passao na requisição
            var produtoBanco = await _context.Produtos.FindAsync(produto.Id);
            //Se não encontrado
            if (produtoBanco == null)
                return NotFound(); //Retorna não encontrado

            //Busca o fornecedor, conforme Id passado e armazena na relação com o produto
            produtoBanco.Fornecedor = await _context.Fornecedores.FindAsync(produto.Fornecedor.Id);
            //Recebe as alterações
            produtoBanco.Descricao = produto.Descricao;
            produtoBanco.Preco = produto.Preco;
            produtoBanco.Quantidade = produto.Quantidade;

            try
            {
                await _context.SaveChangesAsync(); //Salva as alterações
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoExists(produto.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(200); //OK
        }

        // POST: api/Produto
        [HttpPost]
        public async Task<ActionResult<Produto>> PostProduto([FromBody] Produto produto)
        {
            //Busca fornecedor, conforme id passado e armazena na relação com o produto
            produto.Fornecedor = await _context.Fornecedores.FindAsync(produto.Fornecedor.Id);
            #region Validações
            //Quantidade
            if (produto.Quantidade < 1)
                return StatusCode(400, "Quantidade menor do que 1!"); //Bad request
            //Fornecedor
            if (produto.Fornecedor.Id == 0)
                return StatusCode(400, "Fornecedor não informado!"); //Bad request
            #endregion

            _context.Produtos.Add(produto); //Adiciona na tabela
            await _context.SaveChangesAsync(); //Salva no banco

            return CreatedAtAction(nameof(GetProduto), new { id = produto.Id }, produto); //Criado
        }

        // DELETE: api/Produto/id
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduto(int id)
        {
            //Produra o id na tabela de Produtos
            var produto = await _context.Produtos.FindAsync(id);
            //Se não encontrado
            if (produto == null)
            {
                return NotFound(); //Não encontrado
            }

            _context.Produtos.Remove(produto); //Remove da tabela
            await _context.SaveChangesAsync(); //Salva no banco

            return StatusCode(200); //Ok
        }

        private bool ProdutoExists(int id)
        {
            return _context.Produtos.Any(e => e.Id == id);
        }
    }
}
