# Vers�es

- Asp.NET Core 5.0
- Microsoft.EntityFrameworkCore.Tools 5.0.6
- Microsoft.VisualStudio.Web.CodeGeneration.Design 5.0.2
- Npgsql.EntityFrameworkCore.PostgreSQL 5.0.7
- Npgsql.EntityFrameworkCore.PostgreSQL.Design 1.1.0
- Visual Studio Community 2019 16.9.3
- Postman 8.6.1

# Instru��es

## 1 - Configurar arquivo "appsetting.json"
### 1.1 - Alterar a string de conex�o "Acesso_Api", de acordo com o banco utilizado.

## 2 - Rodar a migra��o para cria��o do banco "add-migration (Nome da migra��o)" / "update-database"

## 3 - Executar aplica��o

## 4 - Via Postman, realizar as requisi��es de cada entidade, de acordo com as URLs em "#URLs"
### 4.1 POST - Cadastro
### 4.2 PUT - Atualiza��o
### 4.3 DELETE - Exclus�o
### 4.4 GET - Listagem
### 4.5 GET/ID - Leitura de um registro espec�fico

# URLs
Produto: localhost:44335/api/Produto
Fornecedor: localhost:44335/api/Fornecedor